var path = require('path');
var webpack = require('webpack');
//var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

var externals = {
  'react': 'react',
  'react-dom': 'react-dom',
  'react-helmet': 'react-helmet',
  'react-overlays': 'react-overlays',
  'glamor': 'glamor'
};
var rules = [
  { test: /\.css$/, loader: 'style-loader!css-loader' },
  {
    test: /\.js$/,
    exclude: /node_modules/,
    loader: 'babel-loader',
    query: {
      presets: ['react', 'es2015']
    }
  },
  {
    test: /\.svg$/,
    use: [
      {
        loader: 'svg-url-loader?noquotes'
      },
      {
        loader: 'svgo-loader',
        options: {
          plugins: [
            { cleanupNumericValues: { floatPrecision: 2 } },
            { cleanupListOfValues: { floatPrecision: 2 } },
            { convertPathData: { floatPrecision: 2 } },
            { transformWithOnePath: { floatPrecision: 2 } }
          ]
        }
      }
    ]
  },
  { test: /\.png$/, loader: 'url-loader' },
  { test: /\.gif$/, loader: 'url-loader' },
  { test: /\.jpg$/, loader: 'url-loader' }
];

module.exports = [
  {
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, './lib'),
      filename: 'index.js',
      libraryTarget: 'commonjs'
    },
    externals: externals,
    module: {
      rules: rules
    },
    plugins: [
      //new webpack.optimize.UglifyJsPlugin()
      //new BundleAnalyzerPlugin()
    ]
  },

  {
    entry: './src/TitleScreen.js',
    output: {
      path: path.resolve(__dirname, './lib'),
      filename: 'TitleScreen.js',
      libraryTarget: 'commonjs'
    },
    externals: externals,
    module: {
      rules: rules
    },
    plugins: []
  }
];
