var React = require('react');
var ReactDOM = require('react-dom');

if (SCORM_ENV === 'local') {
  window.API = require('scorm-local')('wbt-101-w');
}

var WBT101W = require('./WBT101W.js').default;
var root = document.getElementById('course');
ReactDOM.render(<WBT101W/>, root);
if (module.hot) {
  module.hot.accept('./WBT101W.js', function () {
    var NewVersion = require('./WBT101W.js').default;
    ReactDOM.render(<NewVersion/>, root);
  });
}
