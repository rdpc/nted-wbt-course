import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M01_L01_S03 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M01_L01_S03.moduleName}
      lesson={M01_L01_S03.lessonName}
      topic="Properties of a web-based course"
      title="Properties of a Web-based course"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column>
        <TextComponent>
          <p>Web-based courses are delivered entirely online. Courses are instructor-less, asynchronous, and self-paced. Students may enroll at any time.</p>

          <p>After a student enrolls, they proceed through a pre-test, the course material, and finally the post-test. As usual, a score of 70% or higher on the post-test passes.</p>

          <p>A few best practices for tests are covered in module five, but the bulk of this course is focused on the course material. That is, the part of the course that is described in the storyboards. The next lesson will give you a brief tour of the NTED course template that will be the home for your course's content.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M01_L01_S03.moduleName = "Module 1: Introduction to Web-based Training";
M01_L01_S03.lessonName = "Introduction";

export default M01_L01_S03;
