import React from 'react';
import { Screen, Column, TextComponent, PictureComponent } from 'nted-wbt-course';
import image from '../../assets/course-footer.png';

const M01_L02_S05 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M01_L02_S05.moduleName}
      lesson={M01_L02_S05.lessonName}
      topic="Course Footer"
      title="Course Footer"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[image]}
    >
      <Column span={6}>
        <TextComponent>
          <p>At the bottom of every screen is the course footer. The contents of the course footer, from left to right:</p>

          <ul style={{ marginBottom: '20px' }}>
            <li><strong>The user prompt.</strong> This will pretty much always read <em>click the next button to continue</em>, except on the last screen where it changes to <em>select a topic from the course menu</em>.</li>
            <li><strong>A progress indicator.</strong> This just tells the user what page they are on, and how many pages there are in the whole course. The title screen is not counted as a page (or you can think of it as page zero).</li>
            <li><strong>Course navigation buttons.</strong> Just a next button to go to the next screen, and a back button to return to the previous screen. The next button is not present on the final screen.</li>
          </ul>
        </TextComponent>
        <PictureComponent
          src={image}
          alt="Course footer."
        />
      </Column>
    </Screen>
  );
}

M01_L02_S05.moduleName = "Module 1: Introduction to Web-based Training";
M01_L02_S05.lessonName = "Course Template";

export default M01_L02_S05;
