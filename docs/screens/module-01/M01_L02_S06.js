import React from 'react';
import { Screen, Column, TextComponent, PictureComponent } from 'nted-wbt-course';
import image from '!file-loader!resize-loader?w=480!../../assets/The_Parthenon_in_Athens.jpg';

const M01_L02_S06 = ({breadcrumb, prefetch}) => {
  let colStyle = {
    display: 'block',
    width: '110px',
    height: '410px',
    margin: '0',
    padding: '0',
    backgroundColor: 'red',
    opacity: '0.07'
  }
  return (
    <Screen
      module={M01_L02_S06.moduleName}
      lesson={M01_L02_S06.lessonName}
      topic="Main Content Area"
      title="Main Content Area"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[image]}
    >
      <Column span={1}>
        <div style={colStyle}/>
      </Column>
      <Column span={1} skip={1}>
        <div style={colStyle}/>
      </Column>
      <Column span={1} skip={2}>
        <div style={colStyle}/>
      </Column>
      <Column span={1} skip={3}>
        <div style={colStyle}/>
      </Column>
      <Column span={1} skip={4}>
        <div style={colStyle}/>
      </Column>
      <Column span={1} skip={5}>
        <div style={colStyle}/>
      </Column>
      <Column span={4}>
        <TextComponent>
          <p>Between the header and footer is the main course content area. The screen title appears at the top of the content area&mdash;on this page the title is <em>Main Content Area</em>&mdash;below which is the screen content.</p>

          <p>The main content area of the course template is divided into <strong>six columns</strong>, and elements such as text and images can span multiple columns. On this screen the columns are shaded so that they can be seen behind the content.</p>

          <p>This particular block of text spans four columns, and the image on the right spans two columns. Constraining content to columns keeps the layouts consistent throughout a course. Six columns makes it easy to split a screen into two halves, or into thirds.</p>
        </TextComponent>
      </Column>
      <Column skip={4} span={2}>
        <PictureComponent
          src={image}
          alt="The Parthenon in Athens, Greece."
          credit="Steve Swayne / CC BY 2.0"
          caption="The ancient Greeks used more than six columns, and look what happened."
        />
      </Column>
    </Screen>
  );
}

M01_L02_S06.moduleName = "Module 1: Introduction to Web-based Training";
M01_L02_S06.lessonName = "Course Template";

export default M01_L02_S06;
