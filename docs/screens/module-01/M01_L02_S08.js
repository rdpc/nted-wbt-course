import React from 'react';
import { Screen, Column, QuestionComponent as Question, Answer } from 'nted-wbt-course';
import graphic from '../../assets/target.svg';

const M01_L02_S08 = ({breadcrumb, prefetch}) => {
  const feedback = (<p>
    Only <strong>the Help tab and the Exit tab are required</strong> to appear in the course header.
    The Resources tab and Glossary tab are only shown if resources or glossary terms are provided.
  </p>);
  return (
    <Screen
      module={M01_L02_S08.moduleName}
      lesson={M01_L02_S08.lessonName}
      topic="Knowledge Check"
      title="Knowledge Check"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <Question
          question="Which of the tabs in the course header are required?"
          feedback={feedback}
        >
          <Answer>The <strong>Resources</strong> tab</Answer>
          <Answer>The <strong>Glossary</strong> tab</Answer>
          <Answer correct>The <strong>Help</strong> tab</Answer>
          <Answer correct>The <strong>Exit</strong> tab</Answer>
        </Question>
      </Column>
      <Column skip={4} span={2}>
        <img src={graphic} style={{width: '100%', height: 'auto', opacity: '0.8'}} alt=""/>
      </Column>
    </Screen>
  );
}

M01_L02_S08.moduleName = "Module 1: Introduction to Web-based Training";
M01_L02_S08.lessonName = "Course Template";

export default M01_L02_S08;
