import React from 'react';
import { Screen, Column, TextComponent, PictureComponent } from 'nted-wbt-course';
import image from '../../assets/course-header.png';

const M01_L02_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M01_L02_S02.moduleName}
      lesson={M01_L02_S02.lessonName}
      topic="Course Header"
      title="Course Header"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column>
        <TextComponent>
          <p>Atop the NTED web-based course template sits the course header. The contents of the header are as follows:</p>

          <ul style={{marginBottom: '20px'}}>
            <li><strong>A FEMA logo</strong> located at the top left.</li>
            <li><strong>The course title</strong> including the NTED catalog number.</li>
            <li><strong>Tabs</strong> linking to various course functions. Available tabs will be described later in this lesson.</li>
            <li><strong>A Menu button</strong> providing access to the course table of contents.</li>
            <li><strong>The module and lesson name</strong> for the current screen appear underneath the tabs at the bottom right of the header.</li>
          </ul>
        </TextComponent>

        <PictureComponent
          src={image}
          alt="NTED course header."
        />
      </Column>
    </Screen>
  );
}

M01_L02_S02.moduleName = "Module 1: Introduction to Web-based Training";
M01_L02_S02.lessonName = "Course Template";

export default M01_L02_S02;
