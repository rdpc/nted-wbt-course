import React from 'react';
import { Screen, Column, TextComponent, PictureComponent } from 'nted-wbt-course';
import image from '!file-loader!resize-loader?w=740!../../assets/title-screen.jpg';

const M01_L02_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M01_L02_S01.moduleName}
      lesson={M01_L02_S01.lessonName}
      topic="Title Screen"
      title="Title Screen"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[image]}
    >
      <Column span={3}>
        <TextComponent>
          <p>Each web-based course starts with the title screen. The title screen simply shows the name of the course, some boilerplate language about the funding, and a button to start the course. If a student has already progressed part way through the course, they will be given an option to restart from the beginning, or continue where they left off.</p>
        </TextComponent>
      </Column>
      <Column skip={3} span={3}>
        <PictureComponent src={image} alt="Course title screen."/>
      </Column>
    </Screen>
  );
}

M01_L02_S01.moduleName = "Module 1: Introduction to Web-based Training";
M01_L02_S01.lessonName = "Course Template";

export default M01_L02_S01;
