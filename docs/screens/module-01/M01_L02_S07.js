import React from 'react';
import { Screen, Column, QuestionComponent, Answer } from 'nted-wbt-course';
import graphic from '../../assets/target.svg';

const M01_L02_S07 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M01_L02_S07.moduleName}
      lesson={M01_L02_S07.lessonName}
      topic="Knowledge Check"
      title="Knowledge Check"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <QuestionComponent
          question="Which element of the course storyboard will not appear anywhere on screen in the web-based course template?"
          feedback={<p>The module and lesson name both appear underneath the tabs in the course header. The screen name appears at the top of the main content area. Only the <strong>topic name</strong> does not appear anywhere on screen.</p>}
        >
          <Answer>Module name</Answer>
          <Answer>Lesson name</Answer>
          <Answer correct>Topic name</Answer>
          <Answer>Screen name</Answer>
        </QuestionComponent>
      </Column>
      <Column skip={4} span={2}>
        <img src={graphic} style={{width: '100%', height: 'auto', opacity: '0.8'}} alt=""/>
      </Column>
    </Screen>
  );
}

M01_L02_S07.moduleName = "Module 1: Introduction to Web-based Training";
M01_L02_S07.lessonName = "Course Template";

export default M01_L02_S07;
