import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M01_L01_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M01_L01_S01.moduleName}
      lesson={M01_L01_S01.lessonName}
      topic="Introduction"
      title="Introduction"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Welcome to <strong>WBT-101-W Best Practices for NTED Web-based Training</strong>. This course is designed to serve two purposes.</p>
          
          <p>First, this course serves as a technical testbed for the library of reusable components we use to build NTED web-based courses. Building courses with reusable components provides many advantages.</p>

          <ul>
            <li>Because the bulk of the code has already been written, it reduces the time required to program a web-based course.</li>
            <li>Improvements to any component in the library are available to all courses based on the library.</li>
            <li>A component library improves consistency across all courses that are using it.</li>
          </ul>

          <p>The second purpose of this course is to provide you, the course developer, with a foundation of best practices and general guidance to build upon when developing your course. The hope is that this will lead to a smoother course development process resulting in an outstanding web-based course!</p>

          <p>In <strong>Module One: Introduction to Web-based Training</strong> you will become acquainted with the general structure of NTED web-based training, including the course template.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M01_L01_S01.moduleName = "Module 1: Introduction to Web-based Training";
M01_L01_S01.lessonName = "Introduction";

export default M01_L01_S01;
