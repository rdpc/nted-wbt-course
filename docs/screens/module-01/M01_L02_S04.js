import React from 'react';
import { Screen, Column, TextComponent, PictureComponent } from 'nted-wbt-course';
import image from '../../assets/course-menu.png';

const M01_L02_S04 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M01_L02_S04.moduleName}
      lesson={M01_L02_S04.lessonName}
      topic="Course Menu"
      title="Course Menu"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[image]}
    >
      <Column span={4}>
        <TextComponent>
          <p>At the bottom left of the course header is the menu button. This button provides a course menu with links to each lesson. The course menu is automatically generated based on the module and lesson names provided for each screen.</p>
        </TextComponent>
      </Column>
      <Column skip={4} span={1}>
        <PictureComponent src={image} alt="Course menu button." />
      </Column>
    </Screen>
  );
}

M01_L02_S04.moduleName = "Module 1: Introduction to Web-based Training";
M01_L02_S04.lessonName = "Course Template";

export default M01_L02_S04;
