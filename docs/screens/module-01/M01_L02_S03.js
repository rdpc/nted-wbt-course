import React from 'react';
import { Screen, Column, TextComponent, PictureComponent } from 'nted-wbt-course';
import image from '../../assets/course-tabs.png';

const M01_L02_S03 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M01_L02_S03.moduleName}
      lesson={M01_L02_S03.lessonName}
      topic="Course Tabs"
      title="Course Tabs"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <TextComponent>
          <p>The course header contains anywhere from two to four tabs, each with a specific purpose.</p>

          <ul>
            <li><strong>The resources tab</strong> displays a list of course resources. Course resources can be documents that are included in the course, or links to other websites. Providing course resources is optional, and the tab will not appear when no resources are specified.</li>
            <li><strong>The glossary tab</strong> provides a list of terms and their definitions when clicked. Using a course glossary is optional, and the tab will only appear if terms are provided.</li>
            <li><strong>The help tab</strong> provides the user with basic instructions on how to use and navigate the course. The help tab is always present and the content is always the same.</li>
            <li><strong>The exit tab</strong> exits the course when clicked. The exit tab is always present.</li>
          </ul>
        </TextComponent>
      </Column>
      <Column skip={4} span={2}>
        <PictureComponent src={image} alt="Course tabs" />
      </Column>
    </Screen>
  );
}

M01_L02_S03.moduleName = "Module 1: Introduction to Web-based Training";
M01_L02_S03.lessonName = "Course Template";

export default M01_L02_S03;
