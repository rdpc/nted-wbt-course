import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';
import graphic from '../../assets/mountain.svg';

const M01_L03_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M01_L03_S01.moduleName}
      lesson={M01_L03_S01.lessonName}
      topic="Conclusion"
      title="Conclusion"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <TextComponent>
          <p>You have completed <strong>Module One: Introduction to Web-based Training</strong> and should now be able to:</p>

          <ul>
            <li>Understand the properties of a web-based course.</li>
            <li>Identify the on-screen elements of a web-based course.</li>
          </ul>
        </TextComponent>
      </Column>
      <Column skip={4} span={2}>
        <img src={graphic} style={{width: '100%', height: 'auto', opacity: '0.8'}} alt=""/>
      </Column>
    </Screen>
  );
}

M01_L03_S01.moduleName = "Module 1: Introduction to Web-based Training";
M01_L03_S01.lessonName = "Conclusion";

export default M01_L03_S01;
