import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M06_L01_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M06_L01_S01.moduleName}
      lesson={M06_L01_S01.lessonName}
      topic="Course Conclusion"
      title="Course Conclusion"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Congratulations! You have completed <strong>WBT-101-W Best Practices for NTED Web-Based Training</strong>.</p>
          
          <p>Pour yourself a drink.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M06_L01_S01.moduleName = "Module 6: Conclusion"
M06_L01_S01.lessonName = "Conclusion"

export default M06_L01_S01;
