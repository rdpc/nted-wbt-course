import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';
import graphic from '../../assets/path-to-goal.svg';

const M04_L01_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M04_L01_S02.moduleName}
      lesson={M04_L01_S02.lessonName}
      topic="Objectives"
      title="Objectives"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <TextComponent>
          <p>Upon completion of <strong>Module Four: Additional Interactive Components</strong> participants will be able to:</p>

          <ul>
            <li>Identify components available for inclusion in a course.</li>
            <li>Describe the considerations involved in the creation of new interactive components.</li>
          </ul>
        </TextComponent>
      </Column>
      <Column skip={4} span={2}>
        <img src={graphic} style={{width: '100%', height: 'auto', opacity: '0.8'}} alt=""/>
      </Column>
    </Screen>
  );
}

M04_L01_S02.moduleName = "Module 4: Additional Interactive Components"
M04_L01_S02.lessonName = "Introduction"

export default M04_L01_S02;
