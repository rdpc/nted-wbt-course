import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M04_L02_S03 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M04_L02_S03.moduleName}
      lesson={M04_L02_S03.lessonName}
      topic="New Components"
      title="New Components"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>The components currently available are just a start. Look for opportunities to create new interactive components as a part of the course development process. Factors that will be considered when deciding whether or not to implement a new component are:</p>

          <ul>
            <li><strong>Value.</strong> Does the component add value to the course, or is it mostly fluff? If the information intended to be conveyed through the proposed component could just as easily be presented as text or image then it may not be the best use of development resources.</li>
            <li><strong>Implementation.</strong> Can the component be implemented in a practical and accessible way? Any components developed must work within FEMA's web-based course template and meet DHS Section 508 standards.</li>
            <li><strong>Reuse.</strong> Is there a reasonable opportunity to reuse the component in other courses? Development resources are always limited, so it is crucial to only create components that can be reused across multiple courses.</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M04_L02_S03.moduleName = "Module 4: Additional Interactive Components"
M04_L02_S03.lessonName = "Additional Components"

export default M04_L02_S03;
