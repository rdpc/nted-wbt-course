import React from 'react';
import { Screen, Column, TextComponent, Narrative, NarrativeSlide as Slide } from 'nted-wbt-course';
import n1 from '!file-loader!resize-loader?h=520!../../assets/DSC_0006.jpg';
const alt1 = 'Two business colleagues talking holding a booklet';
import n2 from '!file-loader!resize-loader?h=520!../../assets/DSC_0026.jpg';
const alt2 = 'Old barn in the country';
import n3 from '!file-loader!resize-loader?h=520!../../assets/50598.jpg';
const alt3 = 'Emergency management professional helping two youths in a emergency triage center';

const M04_L02_S03 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M04_L02_S03.moduleName}
      lesson={M04_L02_S03.lessonName}
      topic="Narrative"
      title="Narrative"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[n1, n2, n3]}
    >
      <Column span={6}>
        <p>The narrative component lets you scroll through a series of images each with some accompanying text.</p>
        <Narrative prompt="Use the arrows to navigate through the slides below.">
          <Slide img={n1} alt={alt1}>
            <h3>Narrative Item 1</h3>
            <p>Narratives are particularly good for showing dialogue between two or more characters, with each step of the conversation being accompanied by an image. This photo story approach can be used to provide context for the learning about to follow, to illustrate real world application of the learning or to show the impact on people when the learning hasn’t been applied correctly.</p>
          </Slide>
          <Slide img={n2} alt={alt2}>
            <h3>Narrative Item 2</h3>
            <p>Narratives can also be used to present case studies, where the different displays are used to set the scene, show the key events and then the outcome.</p>
          </Slide>
          <Slide img={n3} alt={alt3}>
            <h3>Narrative Item 3</h3>
            <p>The narrative can also be used when you want to illustrate the constituent steps that make up a larger process.</p>
          </Slide>
        </Narrative>
      </Column>
    </Screen>
  );
}

M04_L02_S03.moduleName = "Module 4: Additional Interactive Components"
M04_L02_S03.lessonName = "Additional Components"

export default M04_L02_S03;
