import React from 'react';
import { Screen, Column, TextComponent, QuestionComponent, Answer } from 'nted-wbt-course';

const M04_L02_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M04_L02_S01.moduleName}
      lesson={M04_L02_S01.lessonName}
      topic="Question Components"
      title="Question Components"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={3}>
        <TextComponent>
          <p>A <strong>question component</strong> Allows participants to answer a multiple-choice question, and then be provided feedback. More simply put, it lets students test themselves to see how well they understand the course material. See the example on this screen. A question component consists of three parts.</p>

          <ul>
            <li><strong>A question.</strong> There is no question that a question component requires a question. How could anyone question that?</li>
            <li><strong>Answers to choose from.</strong> At least one answer must be correct. When there is more than one correct answer participants must select each of them for their answer to be considered correct.</li>
            <li><strong>Feedback.</strong> When a participant submits their answer, they are told if their answer is correct and presented with some general feedback explaining the correct answer.</li>
          </ul>
        </TextComponent>
      </Column>
      <Column skip={3} span={3}>
        <QuestionComponent
          question="Which of the following items are not part of a question component?"
          feedback={<p>Question components consist of a question, answers, and general feedback. However, question components do not provide <strong>feedback specific to the answer selected</strong> by the participant. Additionally, <strong>providing an incorrect response does not result in an electrical shock</strong>. Yet.</p>}
        >
          <Answer>A question.</Answer>
          <Answer>Answers to choose from.</Answer>
          <Answer correct>Feedback specific to the selected answer.</Answer>
          <Answer>General feedback.</Answer>
          <Answer correct>An electrical shock administered for incorrect responses.</Answer>
        </QuestionComponent>
      </Column>
    </Screen>
  );
}

M04_L02_S01.moduleName = "Module 4: Additional Interactive Components"
M04_L02_S01.lessonName = "Additional Components"

export default M04_L02_S01;
