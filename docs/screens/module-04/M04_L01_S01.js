import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M04_L01_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M04_L01_S01.moduleName}
      lesson={M04_L01_S01.lessonName}
      topic="Introduction"
      title="Introduction"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p><strong>Module Four: Additional Interactive Components</strong> will describe additional components that are available for a course and considerations for creating new ones.</p>

          <p>This module contains the following lessons:</p>

          <ul>
            <li>Lesson 1: Introduction</li>
            <li>Lesson 2: Additional Components</li>
            <li>Lesson 3: Conclusion</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M04_L01_S01.moduleName = "Module 4: Additional Interactive Components"
M04_L01_S01.lessonName = "Introduction"

export default M04_L01_S01;
