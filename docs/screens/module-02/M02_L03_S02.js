import React from 'react';
import { Screen, Column, TextComponent, Footnote } from 'nted-wbt-course';

const M02_L01_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M02_L01_S01.moduleName}
      lesson={M02_L01_S01.lessonName}
      topic="Using Footnotes"
      title="Using Footnotes"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>It is sometimes desirable to include footnotes or references within screen text. Links to footnotes are presented as a superscripted number enclosed in brackets.<Footnote>This is a footnote!</Footnote> Footnotes are numbered per-screen.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M02_L01_S01.moduleName = "Module 2: Text for Web-based Courses"
M02_L01_S01.lessonName = "Writing for Web-based Courses"

export default M02_L01_S01;