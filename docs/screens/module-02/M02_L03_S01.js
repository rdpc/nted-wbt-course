import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M02_L01_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M02_L01_S01.moduleName}
      lesson={M02_L01_S01.lessonName}
      topic="Suggested Word Count"
      title="Suggested Word Count"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>When dividing content into screens, it is important to avoid scrolling whenever possible. Use the following approximated word counts to estimate what content will comfortably fit into available screen space.</p>
          
          <ul>
            <li><strong>About 90 words for two columns.</strong> Two columns span one-third of the width of the screen. There will be a large amount of remaining space for a video or image.</li>
            <li><strong>About 140 words for three columns.</strong> Three columns span one-half of the width of the screen.</li>
            <li><strong>About 160 words for four columns.</strong> Four columns span two-thirds of the width of the screen.</li>
            <li><strong>Around 250 words for six columns.</strong> Six columns span the entire width of the template. Screens with this much text will not have room for images or other content.</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M02_L01_S01.moduleName = "Module 2: Text for Web-based Courses"
M02_L01_S01.lessonName = "Writing for Web-based Courses"

export default M02_L01_S01;