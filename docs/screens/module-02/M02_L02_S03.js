import React from 'react';
import { Screen, Column, TextComponent, Footnote } from 'nted-wbt-course';

const M02_L01_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M02_L01_S01.moduleName}
      lesson={M02_L01_S01.lessonName}
      topic="Block Formatting"
      title="Paragraphs"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Paragraphs are the most basic type of block formatting available when writing content for the web. Paragraphs can be formatted differently in different contexts, but in our course template paragraphs are separated by a full linespace and their first lines are <em>not</em> indented.</p>

          <p>Use paragraphs liberally to break up screen content into smaller chunks, which can be easier for users to digest when reading on the web.<Footnote>For further reading see <a href="https://www.nngroup.com/articles/chunking/" target="_blank">How Chunking Helps Content Processing</a> from the Nielsen Norman Group</Footnote></p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M02_L01_S01.moduleName = "Module 2: Text for Web-based Courses"
M02_L01_S01.lessonName = "Writing for the Web"

export default M02_L01_S01;