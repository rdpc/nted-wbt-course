import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';
import graphic from '../../assets/path-to-goal.svg';

const M02_L01_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M02_L01_S02.moduleName}
      lesson={M02_L01_S02.lessonName}
      topic="Objectives"
      title="Objectives"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <TextComponent>
          <p>Upon completion of <strong>Module Two: Text for Web-based Courses</strong> participants will be able to:</p>

          <ul>
            <li>Implement best practices for writing for the web.</li>
            <li>Identify specific considerations regarding writing for web-based courses.</li>
          </ul>
        </TextComponent>
      </Column>
      <Column skip={4} span={2}>
        <img src={graphic} style={{width: '100%', height: 'auto', opacity: '0.8'}} alt=""/>
      </Column>
    </Screen>
  );
}

M02_L01_S02.moduleName = "Module 2: Text for Web-based Courses"
M02_L01_S02.lessonName = "Introduction"

export default M02_L01_S02;