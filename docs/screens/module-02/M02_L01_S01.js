import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M02_L01_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M02_L01_S01.moduleName}
      lesson={M02_L01_S01.lessonName}
      topic="Introduction"
      title="Introduction"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Text may not sound exciting, but writing will comprise the bulk of a courses meaningful content. <strong>Module Two: Text for Web-based Courses</strong> will cover a few best practices to consider when writing for web-based courses.</p>

          <p>This module contains the following lessons:</p>

          <ul>
            <li>Lesson 1: Introduction</li>
            <li>Lesson 2: Writing for the Web</li>
            <li>Lesson 3: Writing for Web-based Courses</li>
            <li>Lesson 4: Conclusion</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M02_L01_S01.moduleName = "Module 2: Text for Web-based Courses"
M02_L01_S01.lessonName = "Introduction"

export default M02_L01_S01;
