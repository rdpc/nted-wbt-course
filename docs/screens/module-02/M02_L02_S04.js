import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M02_L01_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M02_L01_S01.moduleName}
      lesson={M02_L01_S01.lessonName}
      topic="Block Formatting"
      title="Ordered and Unordered Lists"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Formatting a block of text as a list is another great way to make course content more digestible for readers on the web. In the course template, unordered list items are preceded by bullets. Ordered lists are appropriate for a list of steps or other sequential items, and items are preceded by a number.</p>

          <p>Consider the following best practices when using lists:</p>

          <ul>
            <li><strong>Include at least three items.</strong> When there are only two items consider simply writing two short paragraphs to explain each of them.</li>
            <li><strong>Stick to one level of indentation.</strong> Instead of nesting bullets simply include a sentence or two expanding on the parent item.</li>
            <li><strong>Bottom line up front.</strong> Begin each item with the main point in bold, and then follow up with a sentence or two expanding on the idea. See this very list as an example.</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M02_L01_S01.moduleName = "Module 2: Text for Web-based Courses"
M02_L01_S01.lessonName = "Writing for the Web"

export default M02_L01_S01;