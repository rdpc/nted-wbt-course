import React from 'react';
import { Screen, Column, QuestionComponent as Question, Answer } from 'nted-wbt-course';
import graphic from '../../assets/target.svg';

const M02_L02_S05 = ({breadcrumb, prefetch}) => {
  const feedback = (<p>
    Two types of inline formatting to avoid are <strong>underlines and all caps</strong>.
    Underlined text is likely to be interperted as a link, and all caps is conventionally seen as shouting.
  </p>);
  return (
    <Screen
      module={M02_L02_S05.moduleName}
      lesson={M02_L02_S05.lessonName}
      topic="Knowledge Check"
      title="Knowledge Check"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <Question
          question="Which types of inline formatting should be avoided?"
          feedback={feedback}
        >
          <Answer>Fomatting with <strong>bold</strong> text</Answer>
          <Answer correct>Formatting with ALL CAPS</Answer>
          <Answer>Formatting with <em>italic</em> text</Answer>
          <Answer correct>Formatting with <span style={{textDecoration: 'underline'}}>underlined</span> text</Answer>
        </Question>
      </Column>
      <Column skip={4} span={2}>
        <img src={graphic} style={{width: '100%', height: 'auto', opacity: '0.8'}} alt=""/>
      </Column>
    </Screen>
  );
}

M02_L02_S05.moduleName = "Module 2: Text for Web-based Courses";
M02_L02_S05.lessonName = "Writing for the Web";

export default M02_L02_S05;
