import React from 'react';
import { Screen, Column, TextComponent, Footnote } from 'nted-wbt-course';

const M02_L02_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M02_L02_S01.moduleName}
      lesson={M02_L02_S01.lessonName}
      topic="Inline Formatting"
      title="Inline Formatting"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>When writing for the web, there are two general categories of formatting to consider. The first is <strong>block formatting</strong>, such as paragraphs or lists, which is applied to blocks of text. The second is <strong>inline formatting</strong>, like bold or italics, which can be applied to words or phrases in each block. Stick with bold, and italics when writing for the web.</p>

          <p>On the web, readers scan text before reading every word.<Footnote>See <a href="https://www.nngroup.com/articles/how-users-read-on-the-web/" target="_blank">How Users Read on the Web</a> from the Nielsen Norman Group</Footnote> <strong>Use bold to highlight words, phrases, or key points</strong> that should stand out when a reader scans the text.</p>

          <p>Use italics when you want to emphasize a word or phrase that should <em>not</em> stand out when scanning the screen. For example, the word <em>not</em> in the previous sentence is meaningless outside of the context of the surrounding sentence and shouldn't draw the eye of a reader scanning the page.</p>

          <p>Two types of inline formatting to avoid are underlines and all caps. Underlined text is likely to be interperted as a link,<Footnote marker="[2]">See <a href="https://www.usability.gov/get-involved/blog/2007/05/underlining-links.html" target="_blank">Should All Links be Underlined?</a> on usability.gov.</Footnote> and all caps is conventionally seen as shouting.<Footnote marker="[3]">See <a href="https://en.wikipedia.org/wiki/All_caps#Computing" target="_blank">All caps</a>, on Wikipedia</Footnote></p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M02_L02_S01.moduleName = "Module 2: Text for Web-based Courses"
M02_L02_S01.lessonName = "Writing for the Web"

export default M02_L02_S01;