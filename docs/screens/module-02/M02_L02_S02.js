import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M02_L02_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M02_L02_S02.moduleName}
      lesson={M02_L02_S02.lessonName}
      topic="Using Links"
      title="Using Links"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Links may occasionally included in the text of a course. These links may lead users to outside resources, or they can lead to other locations within the course.</p>

          <p>It is crucial to use appropriate and descriptive text for a link. The <a href="https://www.dhs.gov/sites/default/files/publications/DHS_Section_508_Compliance_Test_Process_for_Applications_0.pdf" target="_blank">DHS Section 508 Test Process for Applications</a> dictates that linked text must provide "a meaningful and unique description that includes destination, function, and or purpose of the link." Link text that does not provide a meaningful description will be marked as not compliant during the Section 508 testing process.</p>

          <p>In general, avoiding non-descriptive link text such as <em>click here</em> will be satisfactory. The Test Process states that "<strong>Links such as <em>learn more</em>, <em>more...</em>, or <em>click here</em> are not meaningful</strong> and do not adequately describe their destination."</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M02_L02_S02.moduleName = "Module 2: Text for Web-based Courses"
M02_L02_S02.lessonName = "Writing for the Web"

export default M02_L02_S02;