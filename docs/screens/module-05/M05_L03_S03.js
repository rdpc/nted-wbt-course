import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M05_L03_S03 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M05_L03_S03.moduleName}
      lesson={M05_L03_S03.lessonName}
      topic="Introduction"
      title="Don't Enumerate Answer Choices"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Because answer choices will be presented to the learner in random order, it doesn't make sense to use labels like <em>A</em>, <em>B</em>, or <em>C</em>. These types of labels can also look out of place with native web controls, because they are unnecessary.</p>

          <p>One consequence of removing labels is that choices like <em>Both B and C</em> are no longer possible.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M05_L03_S03.moduleName = "Module 5: Tests for Web-based Courses"
M05_L03_S03.lessonName = "Best Practices for Writing Questions"

export default M05_L03_S03;
