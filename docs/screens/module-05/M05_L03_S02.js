import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M05_L03_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M05_L03_S02.moduleName}
      lesson={M05_L03_S02.lessonName}
      topic="Introduction"
      title="Use Single-Answer Multiple Choice Questions"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>I'm not sure that I have ever seen a question that <em>wasn't</em> single-answer multiple choice in an RDPC course. Still, just in case you were thinking about using some other type of question, questions for web-based courses should always be multiple choice and only one of those choices should be the correct answer. True or false questions can be represented as a multiple choice question with two choices (<em>true</em> or <em>false</em>).</p>

          <p>Because web-based courses are instructor-less and self-paced, open response questions are impractical to grade. Multiple-answer multiple choice questions can be ambiguous in terms of scoring and are not supported consistantly across LMS software.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M05_L03_S02.moduleName = "Module 5: Tests for Web-based Courses"
M05_L03_S02.lessonName = "Best Practices for Writing Questions"

export default M05_L03_S02;
