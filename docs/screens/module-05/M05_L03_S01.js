import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M05_L03_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M05_L03_S01.moduleName}
      lesson={M05_L03_S01.lessonName}
      topic="Introduction"
      title="Best Practices for Questions"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>While questions from instructor-led tests are often perfectly suitable for web-based courses, we have found that there are a few extra considerations to be aware of when writing questions for web-based tests. This lesson will cover the following best practices.</p>

          <ul>
            <li>Use single-answer multiple choice questions.</li>
            <li>Don't enumerate answer choices.</li>
            <li>Avoid catch-all choices.</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M05_L03_S01.moduleName = "Module 5: Tests for Web-based Courses"
M05_L03_S01.lessonName = "Best Practices for Writing Questions"

export default M05_L03_S01;
