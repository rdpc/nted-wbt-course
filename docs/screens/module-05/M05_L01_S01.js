import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M05_L01_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M05_L01_S01.moduleName}
      lesson={M05_L01_S01.lessonName}
      topic="Introduction"
      title="Introduction"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Tests in web-based courses are similar to their counterparts in instructor-led training. There are, however, a few points to be aware of when writing tests and questions for web-based courses. <strong>Module Five: Tests</strong> will cover a few best practices to consider when creating tests for web-based courses.</p>

          <p>This module contains the following lessons:</p>

          <ul>
            <li>Lesson 1: Introduction</li>
            <li>Lesson 2: Test Structure</li>
            <li>Lesson 3: Best Practices for Writing Questions</li>
            <li>Lesson 4: Conclusion</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M05_L01_S01.moduleName = "Module 5: Tests for Web-based Courses"
M05_L01_S01.lessonName = "Introduction"

export default M05_L01_S01;
