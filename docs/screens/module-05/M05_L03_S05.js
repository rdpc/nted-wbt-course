import React from 'react';
import {
  Screen,
  Column,
  QuestionComponent,
  Answer,
  Blank
} from 'nted-wbt-course';
import graphic from '../../assets/target.svg';

const M05_L03_S05 = ({ breadcrumb, prefetch }) => {
  return (
    <Screen
      module={M05_L03_S05.moduleName}
      lesson={M05_L03_S05.lessonName}
      topic="Knowledge Check"
      title="Knowledge Check"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <QuestionComponent
          question="Which one of the following questions is not appropriate for a web-based training test?"
          feedback={
            <p>
              The phrase "Select all that apply" in the question{' '}
              <strong>
                "Which signs of the zodiac are most compatible with Cappricorns?
                Select all that apply."
              </strong>{' '}
              suggests that multiple answers may be selected. Questions that
              allow participants to select multiple answers should not be used.
            </p>
          }
        >
          <Answer>Whales are actually fish. True or false?</Answer>
          <Answer correct>
            Which signs of the zodiac are most compatible with Cappricorns?
            Select all that apply.
          </Answer>
          <Answer>
            How many emergency support functions are specified in the National
            Support Framework?
          </Answer>
          <Answer>
            A memorandum of <Blank /> is used when two nearby agencies agree to
            jointly host their annual fish fry.
          </Answer>
        </QuestionComponent>
      </Column>
      <Column skip={4} span={2}>
        <img
          src={graphic}
          style={{ width: '100%', height: 'auto', opacity: '0.8' }}
          alt=""
        />
      </Column>
    </Screen>
  );
};

M05_L03_S05.moduleName = 'Module 5: Tests for Web-based Courses';
M05_L03_S05.lessonName = 'Best Practices for Writing Questions';

export default M05_L03_S05;
