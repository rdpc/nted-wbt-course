import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M05_L02_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M05_L02_S01.moduleName}
      lesson={M05_L02_S01.lessonName}
      topic="Introduction"
      title="Test Properties"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Test delivery for web-based courses is a function of the delivery system or LMS (Moodle, for example). This means that the test will <em>not</em> appear in the blue and green FEMA course template that you are looking at now. The exact details and appearance of the test will vary depending on the LMS being used, but there are a few properties of tests that will always be true.</p>

          <ul>
            <li><strong>Pre- and post-tests are the same.</strong> Similarly to pre- and post-tests in instructor-led courses, both will use the same set of questions and answers.</li>
            <li><strong>Questions are in random order.</strong> While the questions are always the same, each attempt will present the questions in random order.</li>
            <li><strong>Answers can be in random order.</strong> Where possible, answers will also be presented in random order. This, combined with randomized question order, provides something of a deterrent to cheating.</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M05_L02_S01.moduleName = "Module 5: Tests for Web-based Courses"
M05_L02_S01.lessonName = "Test Structure"

export default M05_L02_S01;
