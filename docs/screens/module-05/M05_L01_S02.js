import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';
import graphic from '../../assets/path-to-goal.svg';

const M05_L01_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M05_L01_S02.moduleName}
      lesson={M05_L01_S02.lessonName}
      topic="Introduction"
      title="Objectives"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <TextComponent>
          <p>Upon completion of <strong>Module Five: Tests</strong> participants will be able to:</p>

          <ul>
            <li>Recognize how tests are administered as part of a web-based course.</li>
            <li>Identify best practices for writing questions for a web-based course.</li>
          </ul>
        </TextComponent>
      </Column>
      <Column skip={4} span={2}>
        <img src={graphic} style={{width: '100%', height: 'auto', opacity: '0.8'}} alt=""/>
      </Column>
    </Screen>
  );
}

M05_L01_S02.moduleName = "Module 5: Tests for Web-based Courses"
M05_L01_S02.lessonName = "Introduction"

export default M05_L01_S02;
