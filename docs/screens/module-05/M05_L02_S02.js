import React from 'react';
import { Screen, Column, QuestionComponent, Answer } from 'nted-wbt-course';
import graphic from '../../assets/target.svg';

const M05_L02_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M05_L02_S02.moduleName}
      lesson={M05_L02_S02.lessonName}
      topic="Knowledge Check"
      title="Knowledge Check"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <QuestionComponent
          question="Which of the following statements is not true of tests for web-based courses?"
          feedback={<p>The pre- and post-test both use the same questions, and questions are shuffled on each attempt. Answers are also presented in random order if possible. <strong>Tests are not presented as modules or sections inside the FEMA course template</strong>, but are administered using tools provided by the LMS.</p>}
        >
          <Answer>Question order is shuffled on each attempt.</Answer>
          <Answer correct>Questions are presented inside the FEMA WBT template.</Answer>
          <Answer>The pre-test and post-test both use the same questions.</Answer>
          <Answer>Answers are presented in random order whenever possible.</Answer>
        </QuestionComponent>
      </Column>
      <Column skip={4} span={2}>
        <img src={graphic} style={{width: '100%', height: 'auto', opacity: '0.8'}} alt=""/>
      </Column>
    </Screen>
  );
}

M05_L02_S02.moduleName = "Module 5: Tests for Web-based Courses"
M05_L02_S02.lessonName = "Test Structure"

export default M05_L02_S02;
