import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M05_L03_S03 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M05_L03_S03.moduleName}
      lesson={M05_L03_S03.lessonName}
      topic="Introduction"
      title="Avoid Catch-All Choices"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>While choices such as <em>all of the above</em> can be accommodated by pretty much any LMS, they may limit the opportunities for randomization. Please be deliberate and thoughtful about using <em>all of the above</em> type choices when writing test questions.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M05_L03_S03.moduleName = "Module 5: Tests for Web-based Courses"
M05_L03_S03.lessonName = "Best Practices for Writing Questions"

export default M05_L03_S03;
