import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M03_L01_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M03_L01_S01.moduleName}
      lesson={M03_L01_S01.lessonName}
      topic="Introduction"
      title="Introduction"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Images can add visual appeal and context to course material, while video can present material in an especially engaging way. <strong>Module Three: Images and Video</strong> will cover </p>

          <p>This module contains the following lessons:</p>

          <ul>
            <li>Lesson 1: Introduction</li>
            <li>Lesson 2: Images</li>
            <li>Lesson 3: Video</li>
            <li>Lesson 4: Conclusion</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M03_L01_S01.moduleName = "Module 3: Images and Video"
M03_L01_S01.lessonName = "Introduction"

export default M03_L01_S01;
