import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M03_L03_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M03_L03_S02.moduleName}
      lesson={M03_L03_S02.lessonName}
      topic="Closed Captions"
      title="Closed Captions"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>For hearing impaired users, closed captions offer a synchronized written description of the audio content of a video. Written transcripts are not sufficient to meet DHS accessibility standards because they are not synchronized, and thus do not provide an equivalent experience.</p>

          <p>When using content provided from outside sources, check if closed captions are available. It is also sometimes possible to extract captions from YouTube, if the video is available there.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M03_L03_S02.moduleName = "Module 3: Images and Video"
M03_L03_S02.lessonName = "Video"

export default M03_L03_S02;
