import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M03_L02_S05 = ({breadcrumb, prefetch, showResources}) => {
  return (
    <Screen
      module={M03_L02_S05.moduleName}
      lesson={M03_L02_S05.lessonName}
      topic="Writing ALT Text"
      title="Writing ALT Text"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>All images in web-based courses are required to provide appropriate text descriptions. Images that are decorative can simply have empty ALT text, while meaningful images must provide an equivalent text description either through ALT text or other page content. When writing ALT text, consider the following guidelines.</p>

          <ul>
            <li><strong>Be brief when possible.</strong> Try and keep ALT text to a maximum of 250 characters. If an equivalent description requires more characters, consider using an image caption instead.</li>
            <li><strong>Transcribe image text.</strong> The DHS Section 508 Test Process requires that "images that are used to display meaningful text must have the same text in its ALT description."</li>
            <li><strong>Don't be redundant.</strong> There is no need to preface your descriptions with phrases like <em>image of</em> or <em>graphic of</em>.</li>
          </ul>

          <p>See the <a href="#" onClick={showResources}>resources tab</a> for more alt text resources.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M03_L02_S05.moduleName = "Module 3: Images and Video"
M03_L02_S05.lessonName = "Images in Web-based Courses"

export default M03_L02_S05;
