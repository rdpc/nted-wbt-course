import React from 'react';
import { Screen, Column, TextComponent, PictureComponent } from 'nted-wbt-course';
import image from '!file-loader!resize-loader?w=740!../../assets/10693.jpg';

const M03_L02_S04 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M03_L02_S04.moduleName}
      lesson={M03_L02_S04.lessonName}
      topic="Image Attribution and Captions"
      title="Image Attribution and Captions"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[image]}
    >
      <Column span={3}>
        <TextComponent>
          <p>Images placed in a web-based course may include an attribution or caption (or both) when necessary. See this screen for an example of both.</p>
        </TextComponent>
      </Column>
      <Column skip={3} span={3}>
        <PictureComponent
          src={image}
          alt="CDC Headquarters"
          credit={<span>CDC's <a href="https://phil.cdc.gov/phil/home.asp">Image Library</a></span>}
          caption={<span>CDC's <a href="">Roybal campus</a> in Atlanta, Georgia. Consider using photographs like this as a better alternative to including logos in a course.</span>}/>
      </Column>
    </Screen>
  );
}

M03_L02_S04.moduleName = "Module 3: Images and Video"
M03_L02_S04.lessonName = "Images in Web-based Courses"

export default M03_L02_S04;
