import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M03_L02_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M03_L02_S01.moduleName}
      lesson={M03_L02_S01.lessonName}
      topic="Properties of a Good Image"
      title="Properties of a Good Image"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Images can make the screen more visually appealing and provide context for other course content. Still, using no image is better than a using bad image. Consider the following points when considering if an image is appropriate for a course.</p>

          <ul>
            <li><strong>Prefer photographs.</strong> For instance, instead of using a CDC logo as an image, use a photograph of their headquarters featuring a sign with the logo.</li>
            <li><strong>Avoid images that are primarily text.</strong> If an image is just some text saved as an image it probably isn't adding anything to the course that couldn't just be included in the actual course text.</li>
            <li><strong>Avoid stock images.</strong> Try not to lean on stock image sites too heavily for course images. When using stock, take special care to avoid photos that are noticeably staged or cheesy.</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M03_L02_S01.moduleName = "Module 3: Images and Video"
M03_L02_S01.lessonName = "Images in Web-based Courses"

export default M03_L02_S01;
