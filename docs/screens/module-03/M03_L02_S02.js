import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M03_L02_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M03_L02_S02.moduleName}
      lesson={M03_L02_S02.lessonName}
      topic="Resolution Requirements"
      title="Resolution Requirements"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>When possible, please try to provide images with dimensions of at least <strong>1600 pixels wide by 1200 pixels high</strong>. This resolution ensures that images will appear at high quality regardless of their scale or placement in the course template, even on high-density screens like iPads. The course build process will scale down images when required, so don't hesitate to provide the highest resolution images available.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M03_L02_S02.moduleName = "Module 3: Images and Video"
M03_L02_S02.lessonName = "Images in Web-based Courses"

export default M03_L02_S02;
