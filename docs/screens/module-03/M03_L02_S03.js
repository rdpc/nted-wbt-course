import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M03_L02_S03 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M03_L02_S03.moduleName}
      lesson={M03_L02_S03.lessonName}
      topic="Finding Images"
      title="Finding Images"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Finding good images for your web-based course can be a challenge. Here are a few sources for free or reasonably priced images.</p>

          <ul>
            <li><strong>FEMA:</strong> The <a href="https://www.fema.gov/media-library" target="_blank">FEMA Media Library</a> is a great source of public domain photos from FEMA staff.</li>
            <li><strong>The CDC:</strong> The CDC's <a href="https://phil.cdc.gov/phil/home.asp" target="_blank">Public Health Image Library</a> provides access to public domain photos related to many public health topics. They also maintain <a href="https://www.cdc.gov/healthyplaces/images.htm" target="_blank">a list of image collections</a> provided by other federal agencies.</li>
            <li><strong>Flickr:</strong> <a href="https://www.flickr.com/creativecommons/" target="_blank">Flickr</a> allows users to search their catalog of photos licensed under Creative Commons. Be sure that any Creative Commons photos are available under appropriate terms and are attributed properly when required.</li>
          </ul>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M03_L02_S03.moduleName = "Module 3: Images and Video"
M03_L02_S03.lessonName = "Images in Web-based Courses"

export default M03_L02_S03;
