import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';
import graphic from '../../assets/path-to-goal.svg';

const M03_L01_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M03_L01_S02.moduleName}
      lesson={M03_L01_S02.lessonName}
      topic="Objectives"
      title="Objectives"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={4}>
        <TextComponent>
          <p>Upon completion of <strong>Module Three: Images and Video</strong> participants will be able to:</p>

          <ul>
            <li>Understand best practices for using images in web-based courses.</li>
            <li>Understand best practices for using video in web-based courses.</li>
          </ul>
        </TextComponent>
      </Column>
      <Column skip={4} span={2}>
        <img src={graphic} style={{width: '100%', height: 'auto', opacity: '0.8'}} alt=""/>
      </Column>
    </Screen>
  );
}

M03_L01_S02.moduleName = "Module 3: Images and Video"
M03_L01_S02.lessonName = "Introduction"

export default M03_L01_S02;
