import React from 'react';
import { Screen, Column, TextComponent } from 'nted-wbt-course';

const M03_L03_S01 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M03_L03_S01.moduleName}
      lesson={M03_L03_S01.lessonName}
      topic="Video"
      title="Video"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[]}
    >
      <Column span={6}>
        <TextComponent>
          <p>Well produced and relevant video can be a huge asset to a web-based course. The DHS Section 508 test process, however, requires that all video content is made accessible to disabled users through <strong>closed captions</strong> as well as <strong>audio descriptions</strong>. These accessibility features will be described on the following screens.</p>
        </TextComponent>
      </Column>
    </Screen>
  );
}

M03_L03_S01.moduleName = "Module 3: Images and Video"
M03_L03_S01.lessonName = "Video"

export default M03_L03_S01;
