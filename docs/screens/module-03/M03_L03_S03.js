import React from 'react';
import { Screen, Column, TextComponent, PictureComponent } from 'nted-wbt-course';
import image from '!file-loader!resize-loader?w=740!../../assets/Audio_describer_in_live_theater.jpg';

const M03_L03_S02 = ({breadcrumb, prefetch}) => {
  return (
    <Screen
      module={M03_L03_S02.moduleName}
      lesson={M03_L03_S02.lessonName}
      topic="Audio Description"
      title="Audio Description"
      breadcrumb={breadcrumb}
      prefetch={prefetch}
      resources={[image]}
    >
      <Column span={3}>
        <TextComponent>
          <p>While many people are at least somewhat familiar with closed captions, far fewer are aware of audio description. Audio description is an additional audio track for visually impaired users that describes relevant visual details of a video that cannot be understood from the main soundtrack alone.</p>
          
          <p>DHS Section 508 standards require that all video content is accompanied by synchronized audio description. A transcript alone is not sufficient because it is not synchronized, and thus does not provide an equivalent experience.</p>
        </TextComponent>
      </Column>
      <Column skip={3} span={3}>
        <PictureComponent
          src={image}
          alt="An audio describer working in a live theater."
          caption="An audio describer working in a live theater. A small mixer and transmitter are visible."
        />
      </Column>
    </Screen>
  );
}

M03_L03_S02.moduleName = "Module 3: Images and Video"
M03_L03_S02.lessonName = "Video"

export default M03_L03_S02;
