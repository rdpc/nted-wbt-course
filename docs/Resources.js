import React from 'react';
import { Term, Definition } from 'nted-wbt-course';

const Resource = ({href, children}) => <a href={href} target="_blank" rel="noopener">{children}</a>;

const Resources = (props) => (
  <div>
    <p>This course and material is being improved on an ongoing basis. If you encounter an error&mdash;spelling, factual, or otherwise&mdash;please <a href="https://gitlab.com/rdpc/nted-wbt-course/issues" target="_blank" rel="noopener">file an issue</a>.</p>

    <h3>Module Three</h3>

    <ul>
      <li><Resource href="http://www.ssa.gov/accessibility/files/SSA_Alternative_Text_Guide.pdf">SSA Guide: Alternate Text for Images.</Resource> A recommended resource for writing alt text for images.</li>
    </ul>

    <h3>Module Four</h3>

    <ul>
      <li><Resource href="http://www.acb.org/adp/ad.html">The Audio Description Project.</Resource> An initiave of the American Council of the Blind to promote the use of audio description.</li>
    </ul>

    <h3>General Resources</h3>

    <ul>
      <li><Resource href="https://section508.gov">Section508.gov</Resource>. Resources for understanding and implementing the requirements of Section 508.</li>
    </ul>
  </div>
);

export default Resources;
