import React from 'react';
import Stuff from 'nted-wbt-course';
import { Course } from 'nted-wbt-course';
import Glossary from './Glossary';
import Resources from './Resources';
//import CourseResources from './CourseResources';
//import CourseGlossary from './CourseGlossary';
var scorm = require('scorm-api-wrapper');
var screensContext = require.context("./screens", true, /\.js$/);
var screens = screensContext.keys().map(screensContext).map(
  module => React.createElement(module.default, {key: module.default.name})
);

console.log(Stuff);

const WBT101W = (props) => (
  <Course
    catalognum="WBT-101-W"
    title="Best Practices for NTED Web-Based Training"
    resources={<Resources/>}
    glossary={<Glossary/>}
    api={scorm}
  >
    {screens}
  </Course>
);

export default WBT101W;
