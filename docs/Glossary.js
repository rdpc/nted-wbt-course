import React from 'react';
import { Term, Definition } from 'nted-wbt-course';

const Glossary = (props) => (
  <dl>
    <Term>A11y</Term>
    <Definition>A shorthand term for <em>accessibility</em>. The <em>11</em> represents the 11 letters between A and Y.</Definition>

    <Term>NTED</Term>
    <Definition>FEMA's National Training and Education Division.</Definition>

    <Term>WBT</Term>
    <Definition>Web-based training.</Definition>

    <Term>ILT</Term>
    <Definition>Instructor-led training.</Definition>
  </dl>
);

export default Glossary;
