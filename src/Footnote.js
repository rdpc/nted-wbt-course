import React, { Component } from 'react';
import CourseModal from './CourseModal.js';
import { style } from 'glamor';

const sr = style({
  position: 'absolute',
  width: '1px',
  height: '1px',
  padding: '0',
  margin: '-1px',
  overflow: 'hidden',
  clip: 'rect(0,0,0,0)',
  border: '0'
});

export default class Footnote extends Component {
  constructor(props) {
    super(props);
    this.state = {
      noteVisible: false
    }
  }
  hideNote() {
    this.setState({noteVisible: false});
  }
  showNote(event) {
    console.log('shownote');
    this.setState({noteVisible: true});
    event.preventDefault();
  }
  render() {
    var modal = (
      <CourseModal show={this.state.noteVisible} onHide={this.hideNote.bind(this)}>
        {this.props.children}
      </CourseModal>
    );
    return (
      <sup className="footnote"><a href="#ref" onClick={this.showNote.bind(this)}><span className={sr}>Read the footnote </span>{this.props.marker}</a>{modal}</sup>
    );
  }
}

Footnote.defaultProps = {
  marker: "[1]"
};
