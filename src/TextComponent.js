import React from 'react';

const TextComponent = (props) => {
  return (
    <div className="component text-component">
      {props.children}
    </div>
  );
};

export default TextComponent;
