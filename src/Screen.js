import React, { Component } from 'react';
import Helmet from 'react-helmet';
require('./Screen.css');

export default class Screen extends Component {
  render() {
    if (this.props.breadcrumb) {
      return (<span className="breadcrumb">{ this.props.module } <svg focusable="false" width="7" height="8"><polygon points="1,1 1,7 6,4"/></svg> { this.props.lesson }</span>);
    }
    if (this.props.prefetch) {
      var resources = this.props.resources.map(
        resource => ({"rel": "prefetch", "href": resource})
      );
      return (
        <Helmet link={resources}/>
      )
    }
    return (
      <div>
        <h2 className="screen-title">{ this.props.title }</h2>
        <div className="screen-content">
          { this.props.children }
        </div>
      </div>
    );
  }
}

Screen.defaultProps = {
  prompt: "Click the Next button to continue.",
  breadcrumb: false,
  prefetch: false,
  resources: []
};
