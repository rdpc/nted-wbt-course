import React, { Component } from 'react';
require('./Footer.css');

export default class Footer extends Component {
  handleNextScreen() {
    var currentScreen = this.props.currentScreen;
    var totalScreens = this.props.totalScreens;
    var nextScreen = Math.min(totalScreens, currentScreen + 1);
    this.props.setScreen(nextScreen);
  }
  handlePrevScreen() {
    var currentScreen = this.props.currentScreen;
    var totalScreens = this.props.totalScreens;
    var prevScreen = Math.max(0, currentScreen - 1);
    this.props.setScreen(prevScreen);
  }
  render() {
    var currentScreen = this.props.currentScreen;
    var totalScreens = this.props.totalScreens;
    var prevScreen = Math.max(0, currentScreen - 1);
    var nextScreen = Math.min(totalScreens, currentScreen + 1);

    var prompt = <span className="prompt">Click the <strong>Next</strong> button to continue.</span>;
    if (currentScreen === totalScreens) {
      prompt = <span className="prompt">Select a topic from the <strong>course menu</strong>.</span>;
    }
    return (
      <footer className="course-footer">
        {prompt}
        <div className="page">
          <span>Page { currentScreen } of { totalScreens }</span>
          <progress value={ currentScreen } max={ totalScreens }></progress>
        </div>

        <nav>
          <button className="back" onClick={this.handlePrevScreen.bind(this)}>
            <svg focusable="false" width="7" height="8"><polygon points="1,4 6,1 6,7"/></svg> Back
          </button>
          <button className="next" disabled={currentScreen === totalScreens} onClick={this.handleNextScreen.bind(this)}>
            Next <svg focusable="false" width="7" height="8"><polygon points="1,1 1,7 6,4"/></svg>
          </button>
        </nav>
      </footer>
    );
  }
}
