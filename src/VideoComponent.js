import React, { Component } from 'react';

export default class VideoComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDescriptions: false,
      activeDescription: ''
    };
  }

  componentDidMount() {
    console.log(this.adTrack);
    this.adTrack.track.mode = 'hidden';
    this.adTrack.addEventListener('cuechange', this.handleCuechange.bind(this));
  }

  componentWillUnmount() {
    this.adTrack.removeEventListener('cuechange', this.handleCuechange.bind(this));
  }

  toggleDescriptions(event) {
    var current = this.state.showDescriptions;
    this.setState({
      showDescriptions: !current
    });
  }

    handleCuechange(event) {
    var activeCues = event.target.track.activeCues;
    if (activeCues.length) {
      this.setState({activeDescription: activeCues[0].text});
    } else {
      this.setState({activeDescription: ''});
    }
  }

  render() {
    var descriptionStyle = {
      margin: '0',
      lineHeight: '17px'
    };
    var currentDescription;
    if (this.state.showDescriptions) {
      currentDescription = <p style={descriptionStyle} role="alert" aria-live="assertive" aria-atomic="true">{this.state.activeDescription}</p>;
    }
    var adIconStyle = {
      fill: 'white',
      fillOpacity: this.state.showDescriptions ? '1' : '0.5'
    };
    var adIcon = (
      <svg xmlns="http://www.w3.org/2000/svg" width="30" height="20" viewBox="0 0 1500 1000">
        <defs id="defs15" />
        <path d="m 1272.9275,256.84741 c 34.597,-8.697 45.099,25.935 60.226,50.193 33.003,52.921 57.83,120.968 58.229,198.762 0.573,111.59 -47.587,193.47 -92.357,248.955 h -22.078 c -1.521,-9.081 5.774,-17.15 10.03,-24.094 34.155,-55.711 65.807,-132.595 66.258,-220.847 0.514,-101.229 -36.742,-185.875 -80.308,-252.969" style={adIconStyle}/>
        <path d="m 1166.2855,256.84741 c 34.597,-8.697 45.099,25.935 60.226,50.193 33.003,52.921 57.83,120.968 58.229,198.762 0.573,111.59 -47.587,193.47 -92.357,248.955 h -22.078 c -1.521,-9.081 5.774,-17.151 10.03,-24.094 34.155,-55.711 65.808,-132.595 66.258,-220.847 0.514,-101.229 -36.743,-185.875 -80.308,-252.969" style={adIconStyle}/>
        <path d="m 1058.9515,256.84741 c 34.597,-8.697 45.105,25.93 60.226,50.193 33.003,52.921 57.835,120.968 58.229,198.762 0.573,111.59 -47.587,193.47 -92.357,248.955 h -22.078 c -1.521,-9.081 5.776,-17.149 10.03,-24.094 34.155,-55.711 65.807,-132.595 66.258,-220.847 0.514,-101.229 -36.743,-185.875 -80.308,-252.969" style={adIconStyle}/>
        <path d="m 779.45747,375.95541 c 81.537,-2.954 138.036,38.998 146.563,106.411 11.421,90.29 -54.969,149.006 -150.574,142.544 v -242.93 c -0.093,-3.437 1.494,-5.202 4.011,-6.025 m -126.483,-128.494 v 511.964 c 130.7,2.386 231.17,7.101 305.166,-40.153 71.01603,-45.351 126.42803,-133.562 116.45503,-246.945 -10.551,-119.94 -107.23203,-220.72 -232.90003,-230.886 -62.482,-5.054 -186.716,0 -186.716,0 0,0 -2.179,3.17 -2.005,6.02" style={adIconStyle}/>
        <path d="m 504.67147,414.87241 v 165.512 l -104.945,-0.63 104.945,-164.882 z m -396.059,345.575 h 176.678 l 48.183,-66.252 166.47,-0.314 c 0,0 0.169,45.153 0.169,66.566 h 126.485 v -517.985 h -152.584 c -14.665,22.183 -365.401,517.985 -365.401,517.985" style={adIconStyle}/>
      </svg>
    );
    var adButtonStyle = {
      display: 'block',
      backgroundColor: '#555',
      border: '1px solid #666',
      padding: '3px',
      position: 'absolute',
      top: '10px',
      left: '10px',
      height: '38px',
      width: '38px'
    };
    var ccTrack;
    if (this.props.captions) {
      ccTrack = <track ref={(ref) => this.ccTrack = ref} kind="captions" src={this.props.captions} srcLang="en" label="English"/>;
    }
    var adTrack;
    var adDisplay;
    if (this.props.description) {
      adTrack = <track ref={(ref) => this.adTrack = ref} kind="descriptions" src={this.props.description} srcLang="en"/>;
      adDisplay = (
        <div style={{position: 'relative', minHeight: '58px', color: 'white', backgroundColor: '#333', lineHeight: '14px', padding: '10px 10px 10px 58px'}}>
          <button name={'turn ' + (this.state.showDescriptions ? 'off' : 'on') + ' descriptions'} style={adButtonStyle} onClick={this.toggleDescriptions.bind(this)}>{adIcon}</button>
          {currentDescription}
        </div>
      );
    }
    return (
      <div style={{backgroundColor: 'black', lineHeight: '0'}}>
        <video width="100%" controls preload="auto">
          <source type="video/mp4" src={this.props.mp4}/>
          {ccTrack}
          {adTrack}
        </video>
        {adDisplay}
      </div>
    )
  }
}
