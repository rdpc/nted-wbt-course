import React from 'react';

const style = {
  margin: '0 0 20px 0'
};

const Definition = (props) => <dd style={style}>{props.children}</dd>;

export default Definition;
