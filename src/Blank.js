import React from 'react';

const Blank = ({ length = 10 }) => (
  <span aria-label="blank">{'_'.repeat(length)}</span>
);

export default Blank;
