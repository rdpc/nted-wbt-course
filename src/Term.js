import React from 'react';

var style = {
  fontWeight: 'bold'
};

const Term = (props) => <dt style={style}>{props.children}</dt>;

export default Term;
