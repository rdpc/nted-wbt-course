import React, { Component } from 'react';
import { css } from 'glamor';

let inputStyle = css({
  position: 'absolute',
  display: 'block',
  padding: '0',
  opacity: '0.0'
});

let getCheckStyle = (selected, focused, disabled) => {
  let borderColor = disabled ? '#999' : '#369';
  let backgroundColor = selected ? '#369' : 'white';
  return css({
    paddingLeft: '27px',
    display: 'block',
    '::before': {
      content: '""',
      display: 'block',
      position: 'absolute',
      top: '1px',
      left: '3px',
      width: '15px',
      height: '15px',
      backgroundColor: disabled ? '#aaa' : backgroundColor,
      border: `1px solid ${borderColor}`,
      borderRadius: '2px',
      boxShadow: focused ? '0 0 0 3px gold' : 'none',
    },
    '::after': {
      content: '""',
      display: 'block',
      position: 'absolute',
      top: '4px',
      left: '6px',
      transform: 'rotate(-45deg)',
      borderWidth: '0 0 3px 3px',
      borderStyle: 'none none solid solid',
      borderColor: 'white',
      width: '8px',
      height: '4px',
      opacity: selected ? '1' : '0'
    }
  });
}

let getRadioStyle = (selected, focused, disabled) => {
  let borderColor = disabled ? '#999' : '#369';
  let backgroundColor = selected ? '#369' : 'white';
  return css({
    paddingLeft: '27px',
    display: 'block',
    '::before': {
      content: '""',
      display: 'block',
      position: 'absolute',
      top: '1px',
      left: '3px',
      width: '15px',
      height: '15px',
      backgroundColor: disabled ? '#aaa' : backgroundColor,
      border: `1px solid ${borderColor}`,
      borderRadius: '50%',
      boxShadow: focused ? '0 0 0 3px gold' : 'none',
    },
    '::after': {
      content: '""',
      display: 'block',
      position: 'absolute',
      top: '6px',
      left: '8px',
      backgroundColor: 'white',
      width: '7px',
      height: '7px',
      borderRadius: '50%',
      opacity: selected ? '1' : '0'
    }
  });
}

export default class Answer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focused: false
    };
  }

  handleOnFocus(event) {
    this.setState({
      focused: true
    });
  }

  handleOnBlur(event) {
    this.setState({
      focused: false
    });
  }

  answeredCorrectly() {
    return (this.refs.choice.checked === this.props.correct);
  }

  render() {
    let type = this.props.multi ? 'checkbox' : 'radio';
    let id = 'A.' + (Date.now() + Math.random());
    let checked = false;
    if (this.refs.choice) {
      checked = this.refs.choice.checked;
    }
    let getStyle = this.props.multi ? getCheckStyle : getRadioStyle;
    let labelStyle = getStyle(checked, this.state.focused, this.props.submitted);
    return (
      <p style={{position: 'relative'}}>
        <input {...inputStyle}
          id={id}
          disabled={this.props.submitted}
          ref="choice"
          name="answer"
          type={type}
          onChange={this.props.onChange}
          onFocus={this.handleOnFocus.bind(this)}
          onBlur={this.handleOnBlur.bind(this)}/>
        <label {...labelStyle} htmlFor={id}>
          {this.props.children}
        </label>
      </p>
    );
  }
}

Answer.defaultProps = {
  submitted: false,
  correct: false,
  multi: false
};
