import React from 'react';
import { style } from 'glamor';

const PictureComponent = ({ src, alt, credit, caption }) => {
  var creditStyle = style({
    position: 'absolute',
    bottom: '10px',
    right: '10px',
    color: 'white',
    backgroundColor: 'rgba(0,0,0, 0.6)',
    padding: '1px 4px',
    borderRadius: '4px',
    fontSize: '10px',
    textShadow: '1px 1px 1px black',
    '& a': {
      color: 'white !important',
      textDecoration: 'none',
      borderBottom: '1px dotted #999'
    },
    '& a:hover': {
      color: 'white !important',
      backgroundColor: 'black !important',
      borderBottom: '1px solid #ccc'
    },
    '& a:focus': {
      color: 'white !important',
      backgroundColor: 'black !important',
      borderBottom: '1px solid #ccc'
    }
  });
  var figStyle = style({
    position: 'relative',
    borderRadius: '4px',
    overflow: 'hidden',
    boxShadow: '0px 1px 0px rgba(63, 143, 255, 0.33)',
    margin: '0 0 20px 0',
    border: '0px solid rgba(63, 143, 255, 0.25)',
    backgroundColor: 'white',
    ':last-child': {
      marginBottom: '0'
    }
  });
  var imgStyle = {
    maxWidth: '100%',
    height: 'auto',
    display: 'block'
  };
  var captionStyle = style({
    margin: '0',
    padding: '10px',
    fontSize: '10px',
    lineHeight: '10px',
    color: 'white',
    backgroundColor: '#333',
    '& a': {
      color: 'white !important',
      textDecoration: 'none !important',
      borderBottom: '1px dotted #999'
    },
    '& a:hover': {
      color: 'white !important',
      backgroundColor: 'black !important',
      borderBottom: '1px solid #ccc'
    },
    '& a:focus': {
      color: 'white !important',
      backgroundColor: 'black !important',
      borderBottom: '1px solid #ccc'
    }
  });
  if (credit) {
    var creditDisplay = <span {...creditStyle}>Credit: {credit}</span>;
  }
  if (caption) {
    var captionDisplay = <figcaption {...captionStyle}>{caption}</figcaption>;
  }
  return (
    <figure {...figStyle}>
      <div style={{position: 'relative'}}>
        <img style={imgStyle} src={src} alt={alt}/>
        { creditDisplay }
      </div>
      {captionDisplay}
    </figure>
  );
};

PictureComponent.defaultProps = {
  alt: '',
  credit: '',
  caption: ''
}

export default PictureComponent;
