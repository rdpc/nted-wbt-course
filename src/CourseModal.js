import React, { Component } from 'react';
import { Modal } from 'react-overlays';
require('./CourseModal.css');

export default class CourseModal extends Component {
  render() {
    var header;
    if (this.props.title) {
      header = (<h1 className="modal-header">{ this.props.title }</h1>);
    }
    var contentStyle = {
      maxHeight: '400px',
      overflow: 'auto'
    };
    return (
      <Modal
        className="modal"
        backdropClassName="modal-backdrop"
        show={ this.props.show }
        onHide={ this.props.onHide }>
        <div className="modal-dialog">
          { header }
          <div className="modal-content" style={contentStyle} tabIndex="0">
            { this.props.children }
          </div>
          <div className="modal-footer">
            <button onClick={ this.props.onHide }>Close</button>
          </div>
        </div>
      </Modal>
    );
  }
}
