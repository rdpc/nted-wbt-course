import React from 'react';
import { css } from 'glamor';

const containterStyle = css({
  position: 'relative',
  height: '100%',
  margin: 0
});

const imgStyle = css({
  position: 'absolute',
  width: '50%',
  height: '100%',
  overflow: 'hidden'
});

// Possibly scale based on image dimensions.
// See http://stackoverflow.com/questions/39092859/get-dimensions-of-image-with-react#answer-39094233

const textStyle = css({
  position: 'absolute',
  width: '50%',
  height: '100%',
  right: '0px',
  backgroundColor: 'rgba(51,102,153, 0.9)',
  padding: '20px',
  color: 'white',
  '& h3': {
    color: 'white !important',
    marginTop: '0 !important'
  }
});

const NarrativeSlide = ({ img, alt, widen=false, children }) => {
  let ident = `A-${Date.now()}`;
  return (
    <figure {...containterStyle}>
      <div {...imgStyle}>
        { widen ?
          <img src={img} alt={alt} aria-describedby={ident} width="100%" /> :
          <img src={img} alt={alt} aria-describedby={ident} height="100%" /> }
      </div>
      <figcaption id={ident} {...textStyle}>
        {children}
      </figcaption>
    </figure>
  );
};

export default NarrativeSlide;
