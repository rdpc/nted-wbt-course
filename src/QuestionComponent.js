import React, { Component } from 'react';
import CourseModal from './CourseModal.js';

const fieldsetStyle = {
  border: 'none',
  margin: '0',
  padding: '0'
};

const legendStyle = {
  margin: '0 0 20px 0',
  padding: '0',
  width: '100%',
  color: 'inherit'
};

export default class QuestionComponent extends Component {
  constructor(props) {
    super(props);
    this.choices = [];
    this.state = {
      submitted: false,
      feedback: false,
      correct: false,
      answered: false
    };
  }

  submitAnswers() {
    this.setState({
      submitted: true,
      feedback: true
    });
  }

  checkAnswers(event) {
    var correct = this.choices.every(choice => choice.answeredCorrectly());
    this.setState({
      correct: correct,
      answered: true
    });
  }

  showFeedback() { this.setState({ feedback: true }) }
  hideFeedback() { this.setState({ feedback: false }) }

  render() {
    let numCorrect = React.Children.toArray(this.props.children).map(
      child => child.props.correct ? 1 : 0
    ).reduce(
      (prev, curr) => prev + curr
    );
    let multi = numCorrect > 1;
    var answers = React.Children.toArray(this.props.children).map(
      (answer, idx) => React.cloneElement(answer, {
        multi: multi,
        submitted: this.state.submitted,
        ref: choice => this.choices[idx] = choice,
        onChange: this.checkAnswers.bind(this)
      })
    );
    var feedback = (
      <CourseModal
        show={this.state.feedback}
        onHide={this.hideFeedback.bind(this)}>
        { this.state.correct ? this.props.correctFeedback : this.props.incorrectFeedback }
        { this.props.feedback }
      </CourseModal>
    );
    return (
      <form onSubmit={e => e.preventDefault()}>
        {feedback}
        <fieldset style={fieldsetStyle}>
          <legend style={legendStyle}>{this.props.question}</legend>
          {answers}
        </fieldset>
        <p><button type="button" disabled={!this.state.answered} onClick={this.submitAnswers.bind(this)}>
          { this.state.submitted ? 'View feedback' : 'Submit answer' }
        </button></p>
      </form>
    );
  }
}

QuestionComponent.defaultProps = {
  incorrectFeedback: (<p>Your answer is <strong style={{color: 'red'}}>incorrect</strong>.</p>),
  correctFeedback: (<p>Your answer is <strong style={{color:'green'}}>correct</strong>.</p>)
};
