module.exports = {
  Course: require('./Course.js').default,
  Screen: require('./Screen.js').default,
  Column: require('./Column.js').default,
  Footnote: require('./Footnote.js').default,
  TextComponent: require('./TextComponent.js').default,
  PictureComponent: require('./PictureComponent.js').default,
  VideoComponent: require('./VideoComponent.js').default,
  QuestionComponent: require('./QuestionComponent.js').default,
  Answer: require('./Answer.js').default,
  Blank: require('./Blank.js').default,
  Term: require('./Term.js').default,
  Definition: require('./Definition.js').default,
  TitleScreen: require('./TitleScreen.js').default,
  Narrative: require('./Narrative.js').default,
  NarrativeSlide: require('./NarrativeSlide.js').default
};
