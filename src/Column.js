import React from 'react';

const Column = ({ children, span, skip}) => {
  const styles = {
    position: 'absolute',
    top: '0px',
    bottom: '0px',
    overflowX: 'visible',
    overflowY: 'auto'
  };
  styles.width = (span * 110 + (span - 1) * 20) + 'px';
  styles.left = (skip * 130) + 'px';
  return (<div style={styles}>{children}</div>);
};

Column.defaultProps = {
  span: 6,
  skip: 0
};

Column.propTypes = {
  span: React.PropTypes.number.isRequired,
  skip: React.PropTypes.number.isRequired
}

export default Column;
