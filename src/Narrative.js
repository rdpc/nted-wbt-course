import React from 'react';
import { css } from 'glamor';

const btnStyle = css({
  display: 'block',
  position: 'absolute',
  top: '40%',
  bottom: '40%',
  width: '35px',
  padding: '0px',
  border: 'none',
  cursor: 'pointer',
  backgroundColor: 'rgba(255, 255, 255, 0.5)',
  ':hover': {
    backgroundColor: 'white'
  },
  '[disabled]': {
    display: 'none'
  }
});

const prevStyle = css({
  left: '0',
  borderRadius: '0 5px 5px 0'
});

const arrowStyle = css({
  display: 'block',
  overflow: 'hidden',
  margin: '0 auto',
  width: '20px',
  height: '20px',
  borderColor: '#336699',
  borderStyle: 'solid',
  color: 'transparent',
  transform: 'rotate(45deg)'
});

const prevArrowStyle = css({ borderWidth: '0 0 7px 7px' });
const nextArrowStyle = css({ borderWidth: '7px 7px 0 0' });

const nextStyle = css({
  right: '50%',
  borderRadius: '5px 0 0 5px'
});

const progressStyle = css({
  background: '#369',
  border: 'none',
  width: '12px',
  height: '12px',
  lineHeight: '0',
  margin: '0 4px',
  padding: '0',
  borderRadius: '50%',
  color: 'transparent',
  overflow: 'hidden',
  cursor: 'pointer'
});

const inactiveProgressStyle = css({
  opacity: '0.5'
});

export default class Narrative extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 0
    };
  }

  nextSlide() {
    this.setState((prevState, props) => {
      let currentSlide = prevState.currentSlide;
      let totalSlides = React.Children.count(props.children);
      let nextSlide = Math.min(totalSlides - 1, currentSlide + 1);
      return { currentSlide: nextSlide };
    });
  }

  prevSlide() {
    this.setState((prevState, props) => {
      let currentSlide = prevState.currentSlide;
      let prevSlide = Math.max(0, currentSlide - 1);
      return { currentSlide: prevSlide };
    });
  }

  setSlide(idx) {
    this.setState((prevState, props) => ({ currentSlide: idx }));
  }

  render() {
    let height = this.props.height;
    let currentSlide = React.Children.toArray(this.props.children)[
      this.state.currentSlide
    ];
    let totalSlides = React.Children.count(this.props.children);
    let progress = React.Children
      .toArray(this.props.children)
      .map((screen, idx) => {
        if (idx === this.state.currentSlide) {
          return (
            <button
              {...progressStyle}
              key={idx}
              onClick={() => this.setSlide(idx)}
            >
              Slide {idx + 1}
            </button>
          );
        }
        return (
          <button
            {...inactiveProgressStyle}
            {...progressStyle}
            key={idx}
            onClick={() => this.setSlide(idx)}
          >
            Slide {idx + 1}
          </button>
        );
      });
    return (
      <div>
        <p>{this.props.prompt}</p>
        <div style={{ height: `${height}px`, position: 'relative' }}>
          {currentSlide}
          <button
            aria-label="Previous slide"
            disabled={this.state.currentSlide === 0}
            {...btnStyle}
            {...prevStyle}
            onClick={this.prevSlide.bind(this)}
          >
            <span {...arrowStyle} {...prevArrowStyle}>Previous slide</span>
          </button>
          <button
            aria-label="Next slide"
            disabled={this.state.currentSlide === totalSlides - 1}
            {...btnStyle}
            {...nextStyle}
            onClick={this.nextSlide.bind(this)}
          >
            <span {...arrowStyle} {...nextArrowStyle}>Next slide</span>
          </button>
        </div>
        <div style={{ textAlign: 'center', marginTop: '8px' }}>{progress}</div>
      </div>
    );
  }
}

Narrative.defaultProps = {
  height: 260
};
