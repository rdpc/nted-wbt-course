import React from 'react';
import logo from './FEMA_logo.svg';
import bg from './title-screen-flag.jpg';

export default function TitleScreen({catalognum, title, funding, loading, previousLocation}) {
  var style = {
    boxSizing: 'border-box',
    width: '800px',
    height: '600px',
    padding: '40px',
    position: 'relative',
    backgroundColor: '#236CA1',
    backgroundImage: 'url(' + bg + ')',
    backgroundSize: 'cover',
    color: 'white',
    fontFamily: 'Arial'
  };
  var disclaimer = `This program was supported by Cooperative Agreement Number ${funding} administered by the U.S. Department of Homeland Security, Training and Education Division. Points of view or opinions in this program are those of the author(s) and do not represent the position or policies of the U.S. Department of Homeland Security.`;
  var titleStyle = {
    margin: '0',
    padding: '40px 0 0',
    fontFamily: 'Times New Roman',
    fontSize: '40px',
    textShadow: '0px 1px 5px #236CA1'
  };
  var infoStyle = {
    position: 'absolute',
    margin: '0',
    padding: '0',
    width: '400px',
    bottom: '40px',
    left: '40px',
    color: '#DEF'
  };
  var actionBoxStyle = {
    position: 'absolute',
    bottom: '40px',
    right: '40px',
    fontSize: '16px'
  };
  var actionStyle = {
    padding: '12px',
    borderRadius: '4px',
    backgroundColor: '#FC0',
    color: '#158',
    textDecoration: 'none',
    fontSize: '16px',
    fontWeight: 'bold',
    boxShadow: '0px 1px 10px #236CA1'
  };
  var alternateActionStyle = {
    padding: '11px',
    borderRadius: '4px',
    border: '1px solid white',
    backgroundColor: 'rgba(35, 108, 161, 0.5)',
    color: 'white',
    textDecoration: 'none',
    fontSize: '16px',
    boxShadow: '0px 1px 10px #236CA1'
  };
  var loadingStyle = {
    position: 'absolute',
    bottom: '40px',
    right: '40px',
    padding: '12px',
    color: 'white',
    fontSize: '16px',
    fontWeight: 'bold',
    textShadow: '1px 1px 2px #236CA1'
  };
  var pStyle = {
    margin: '20px 0 0',
    fontSize: '16px'
  };
  let continueButton = (
    <span style={actionBoxStyle}>
      <a style={actionStyle} href={'#' + previousLocation}>Continue</a>
      <span style={{margin: '0 12px'}}>or</span>
      <a style={alternateActionStyle} href="#1">Restart course</a>
    </span>
  );
  let startButton = (
    <span style={actionBoxStyle}>
      <a style={actionStyle} href="#1">Start course</a>
    </span>
  );
  let loadingButton = <span style={loadingStyle}>Course loading...</span>;
  let action = loadingButton;
  if (!loading) {
    action = previousLocation ? continueButton : startButton;
  }
  return (
    <div style={style}>
      <h1 style={titleStyle}>{catalognum} {title}</h1>

      <div style={infoStyle}>
        <p style={pStyle}><img src={logo} alt="U.S. Department of Homeland Security - Federal Emergency Management Agency seal" width="200"/></p>
        <p style={pStyle}><small>{disclaimer}</small></p>
      </div>
      
      {action}
    </div>
  );
}

TitleScreen.defaultProps = {
  catalognum: 'WBT-000-W',
  title: 'Web-Based Training',
  funding: 'xxxx-xx-xx-xxxx',
  loading: false,
  previousLocation: 0
};
