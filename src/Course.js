import React, { Component } from 'react';
import TitleScreen from './TitleScreen.js';
import Footer from './Footer.js';
import { Modal } from 'react-overlays';
import CourseModal from './CourseModal.js';
var style = require('./style.css');
var logo = require('./FEMA_logo.svg');
var bgImage = require('!url-loader!./flag-bg.jpg');

export default class Course extends Component {
  constructor(props) {
    super(props);
    var currentScreen = 0;
    if (! isNaN(parseInt(location.hash.slice(1)))) {
      currentScreen = parseInt(location.hash.slice(1));
    }
    this.state = {
      currentScreen: currentScreen,
      showHelp: false,
      showResources: false,
      showGlossary: false,
      showMenu: false,
      previousLocation: 0
    };
    var screens = React.Children.toArray(this.props.children);
    this.contents = screens.reduce((prev, screen, idx) => {
      if (screen.type.moduleName) {
        if (!prev.length || prev[prev.length - 1].name !== screen.type.moduleName) {
          prev.push({
            name: screen.type.moduleName,
            lessons: []
          });
        }
      }
      if (screen.type.lessonName) {
        var currentModule = prev[prev.length - 1];
        if (!currentModule.lessons.length) {
          currentModule.lessons.push({
            name: screen.type.lessonName,
            screen: idx + 1
          });
        } else if (screen.type.lessonName !== currentModule.lessons[currentModule.lessons.length - 1].name) {
          currentModule.lessons.push({
            name: screen.type.lessonName,
            screen: idx + 1
          });
        }
      }
      return prev;
    }, []);
  }

  getMenu() {
    var linkStyle = {
      color: '#06c'
    };
    return this.contents.map(module => {
      return (
        <div key={module.name}>
          <h2 style={{color: '#333'}}>{module.name}</h2>
          <ul style={{marginBottom: '20px'}}>
            {module.lessons.map(lesson => (
              <li key={lesson.screen}>
                <a style={linkStyle} onClick={this.hideMenu.bind(this)} href={'#' + lesson.screen}>{lesson.name}</a>
              </li>)
            )}
          </ul>
        </div>
      );
    });
  }

  setScreen(screenNumber) {
    location.hash = '' + screenNumber;
  }

  updatePage() {
    if (! isNaN(parseInt(location.hash.slice(1)))) {
      if (this.props.api) {
        this.props.api.setScormValue('cmi.core.lesson_location', location.hash.slice(1));
        let currentScreen = parseInt(location.hash.slice(1));
        let totalScreens = React.Children.count(this.props.children);
        if (currentScreen === totalScreens) {
          this.props.api.setScormValue('cmi.core.lesson_status', 'completed');
          this.props.api.setScormValue('cmi.core.score.min', 0);
          this.props.api.setScormValue('cmi.core.score.max', 100);
          this.props.api.setScormValue('cmi.core.score.raw', 100);
        }
        this.props.api.commit();
      }
      this.setState({
        currentScreen: parseInt(location.hash.slice(1))
      });

    }
    document.activeElement.blur();
  }

  componentDidMount() {
    this.props.api.initialize();
    var lessonStatus = this.props.api.getScormValue('cmi.core.lesson_status');
    if (lessonStatus === 'not attempted') {
      this.props.api.setScormValue('cmi.core.lesson_status', 'incomplete');
      this.props.api.commit();
    }

    var lessonLocation = this.props.api.getScormValue('cmi.core.lesson_location');
    if (!isNaN(lessonLocation)) {
      this.setState({
        previousLocation: parseInt(lessonLocation)
      });
    }

    window.addEventListener('hashchange', this.updatePage.bind(this), false);
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.updatePage.bind(this));
    this.props.api.finish();
  }

  showHelp() { this.setState({ showHelp: true }); }
  hideHelp() { this.setState({ showHelp: false }); }

  showResources(event) {
    this.setState({ showResources: true });
    event.preventDefault();
  }
  hideResources() { this.setState({ showResources: false }); }

  showGlossary() { this.setState({ showGlossary: true }); }
  hideGlossary() { this.setState({ showGlossary: false }); }

  showMenu() { this.setState({ showMenu: true }); }
  hideMenu() { this.setState({ showMenu: false }); }

  onCourseExit() {
    this.props.api.finish();
    window.close();
  }

  skipToContent(event) {
    console.log(event);
    this.refs.screens.focus();
    event.preventDefault();
  }

  render() {
    if (this.state.currentScreen === 0) {
      return (
        <TitleScreen
          catalognum={this.props.catalognum}
          title={this.props.title}
          funding={this.props.funding}
          previousLocation={this.state.previousLocation}/>
      );
    }
    let currentLocation = '#' + this.state.currentScreen;
    var resourceTab;
    if (this.props.resources) {
      resourceTab = <button onClick={this.showResources.bind(this)}>Resources</button>;
    }
    var glossaryTab;
    if (this.props.glossary) {
      glossaryTab = <button onClick={this.showGlossary.bind(this)}>Glossary</button>;
    }
    var totalScreens = React.Children.count(this.props.children);
    var screen = React.Children.toArray(this.props.children)[this.state.currentScreen - 1];
    if (this.state.currentScreen < totalScreens) {
      var nextScreen = React.Children.toArray(this.props.children)[this.state.currentScreen];
      var prefetch = React.cloneElement(nextScreen, {prefetch: true});
    }
    var breadcrumb = React.cloneElement(screen, {breadcrumb: true});
    var content = React.cloneElement(screen, {showResources: this.showResources.bind(this)});
    return (
      <div>
        <header>
          <CourseModal
            title="Menu"
            show={ this.state.showMenu }
            onHide={ this.hideMenu.bind(this) }>
            {this.getMenu()}
          </CourseModal>
          <CourseModal
            title="Resources"
            show={ this.state.showResources }
            onHide={ this.hideResources.bind(this) }>
            {this.props.resources}
          </CourseModal>
          <CourseModal
            title="Glossary"
            show={ this.state.showGlossary }
            onHide={ this.hideGlossary.bind(this) }>
            {this.props.glossary}
          </CourseModal>
          <CourseModal
            title="Help"
            show={ this.state.showHelp }
            onHide={ this.hideHelp.bind(this) }>
            <h2>Navigating this Course</h2>
            <h3>Using Navigational Controls</h3>
            <ul>
              <li>Select <strong>Back</strong> to return to the previous screen.</li>
              <li>Select <strong>Next</strong> to go to the next screen.</li>
              <li>Select <strong>Exit</strong> to close the course.</li>
              <li>Select <strong>Menu</strong> to view the topics in the course and then select a topic from the list.</li>
            </ul>
            <h3>Using Keyboard Commands</h3>
            <ul>
              <li>Type the <strong><kbd>Tab</kbd></strong> key on your keyboard to access hyperlinks and linked graphics.</li>
            </ul>
          </CourseModal>
          <div>
            <div className="logo">
              <img src={logo} alt="U.S. Department of Homeland Security - Federal Emergency Management Agency seal"/>
            </div>
            <h1 className="course-title">{ this.props.catalognum } { this.props.title }</h1>
            <a href="#screens" tabIndex="0" className="skip-link" onClick={ this.skipToContent.bind(this) }>Skip to main content</a>
            <div className="tabs">
              {resourceTab}
              {glossaryTab}
              <button onClick={this.showHelp.bind(this)}>Help</button>
              <button onClick={this.onCourseExit.bind(this)}>Exit</button>
            </div>
          </div>
          <nav>
            <button onClick={this.showMenu.bind(this)} className="menu">
              <svg focusable="false" width="10" height="10" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1664 1344v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45zm0-512v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45zm0-512v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45z"/></svg>
              <span>Menu</span>
            </button>
            
            { breadcrumb }
          </nav>
        </header>

        <div tabIndex="-1" id="screens" ref="screens" style={{
            backgroundImage: 'url(' + bgImage + ')',
            backgroundSize: 'cover'
          }}>
          { content }
        </div>

        <Footer
          setScreen={this.setScreen.bind(this)}
          currentScreen={this.state.currentScreen}
          totalScreens={totalScreens}/>
        { prefetch }
      </div>
    );
  }
}
