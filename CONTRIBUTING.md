## Setting Up ##

The first thing you'll need to do to contribute to this project is set up your development environment.

### Install Git ###

Obviously, you'll need to install [Git](https://git-scm.com/) for version control. You can find a version of Git for your platform on the [Git download page](https://git-scm.com/downloads).

If you'd like to become familiar with Git, you can try the free, 15 minute [Try Git](https://try.github.io/) online course.

### Install Node.js ###

The development and build tools for this project depend on [Node.js](https://nodejs.org/en/). Download and install the latest LTS version (v6.11.0 as of 6/9/2017).

### Install Visual Studio Code ###

While **not strictly necessary**, It is suggested that contributors use [Visual Studio Code](https://code.visualstudio.com/) as their development environment. Download and install the version for your platform.

If you choose to use VS Code, it is also strongly recommended that you install the [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense) and [Prettier]() extensions to ease development.

### Add your SSH key to GitLab ###

Follow the instructions in the [GitLab documentation](https://gitlab.com/help/ssh/README) to generate a SSH key (if needed) and add it to GitLab.

## Starting Development ##

Once You've set everything up you are ready to begin development!

### Clone the project ###

The command `git clone git@gitlab.com:rdpc/nted-wbt-course.git` will pull the current version of the project into the sub-directory `nted-wbt-course` of your current directory.

### Install dependencies ###

Running the command `npm install` inside the `nted-wbt-course` directory will install all of the Node.js dependencies required for the project to build and run.

### Start the Dev Server ###

Once the dependencies are installed, you can use `npm start` to start the development server. Once the dev server is running, you can see the current state of the project at <http://localhost:8080/> in your browser. Any time you save a change to a project file the browser will automatically reload the latest version. Pretty cool!
