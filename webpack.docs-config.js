var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './docs/entry.js',
  output: {
    path: path.resolve(__dirname, './public'),
    filename: 'bundle.js'
  },
  resolve: {
    alias: {
      "nted-wbt-course": path.resolve('./src/index.js')
    }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: ['react', 'es2015']
        }
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-url-loader?noquotes'
          },
          {
            loader: 'svgo-loader',
            options: {
              plugins: [
                { cleanupNumericValues: { floatPrecision: 2 } },
                { cleanupListOfValues: { floatPrecision: 2 } },
                { convertPathData: { floatPrecision: 2 } },
                { transformWithOnePath: { floatPrecision: 2 } }
              ]
            }
          }
        ]
      },
      { test: /\.png$/, loader: 'file-loader' },
      { test: /\.gif$/, loader: 'file-loader' },
      {
        test: /\.jpg$/,
        use: [
          {
            loader: 'file-loader'
          },
          {
            loader: 'resize-loader'
          }
        ]
      },
      { test: /\.pdf$/, loader: 'file-loader' },
      { test: /\.docx$/, loader: 'file-loader' },
      { test: /\.mp4$/, loader: 'file-loader' },
      { test: /\.vtt$/, loader: 'file-loader' }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      SCORM_ENV: JSON.stringify(process.env.SCORM_ENV || 'local'),
    }),
    new HtmlWebpackPlugin({
      template: './docs/index.html'
    })
  ]
};
